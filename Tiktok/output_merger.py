import pandas as pd
import os
import csv
import glob
from natsort import natsorted

csv.field_size_limit(13107200)
combined_name = 'data_all'
path = 'output' + os.sep
combined = f'{path}{combined_name}'

csv_files = natsorted(glob.glob(path + '*.csv'))
json_files = natsorted(glob.glob(path + '*.json'))
print()
print('The following files are about to be merged. Are the list of files complete and sorted well?')

print('json files:')
for item in json_files:
    print(item)
print()
print(f'number of files found = {len(json_files)}')
val = input("Enter y or yes if the list is correct: ")
print(f'You answered: {val.lower().strip()}')

if val.lower().strip() == 'y' or val.lower().strip() == 'yes':
    pass
else:
    print('aborting...')
    quit()

print('merging...')

# Combining JSON
dfs = []
for fn in json_files:
    data_fn = fn
    print(f'Reading {data_fn}')
    df = pd.read_json(data_fn, lines=True)
    dfs.append(df)
print()
print('Merging all... This will take a few minutes...')
df_all = pd.concat(dfs, axis=0)
df_all.reset_index(drop=True, inplace=True)

data_fn = combined + '.json'
df_all.to_json(data_fn, orient='records', lines=True, force_ascii=False)
print(f'Merged json saved in {data_fn}')

# Save it as csv for inspection
df_all = df_all.applymap(
    lambda x: str(x).replace('\n', ' '))  # Replace newlines to not affect importing them in Spreadsheets
df_all = df_all.applymap(lambda x: str(x).replace('\r', ' '))

data_fn = combined + '.csv'
df_all.to_csv(data_fn, encoding='utf-8-sig', sep='ζ', index=False)
print(f'Merged csv saved in {data_fn}')
