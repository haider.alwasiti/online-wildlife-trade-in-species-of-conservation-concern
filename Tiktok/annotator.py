import argparse
import pandas as pd
import csv
from tqdm import tqdm
from pathlib import Path

tqdm.pandas()
csv.field_size_limit(13107200)

parser = argparse.ArgumentParser()
parser.add_argument('--annotate_encrypted_files', '-enc', dest='annotate_encrypted_files', type=lambda s: s.lower() in ['true', 't', 'yes', '1'],
                    help='Annotate the encrypted files. This argument is required.', required=True)
args = parser.parse_args()

annotate_encrypted_files = args.annotate_encrypted_files
subsample_rows = 0  # 0 for all rows

# In other platform search scripts filter terms intentionally preceded with space for better relevance search.
# Here with TikTok no spaces added, because tiktok heavily dependant on hashtags that the word sell for example
# written as #sell.
relevance_filter_terms = ['sell', 'sale', 'trade', 'usd', 'peso', 'cost', 'price',
'alibaba', 'reddit', 'amazon', 'lazada', 'shopee', 'carousell', 'ebay',
'ibenta', 'pagbebenta', 'kalakal', 'gastos', 'presyo']

columns_to_search = ['hashtag_search', 'description']

if annotate_encrypted_files:
    path = Path().absolute().parent / 'output_encrypted/tiktok'
    data_fn = path / 'Tiktok.json'
else:
    path = Path('output')
    data_fn = path / 'Tiktok_cleaned.json'


print('Reading data input...')
df = pd.read_json(data_fn, lines=True)
df_names = pd.read_json('input/common_names.json', lines=True)

if subsample_rows > 0:
    df = df.head(subsample_rows)


df.reset_index(drop=True, inplace=True)

# Adding relevance columns
df.insert(len(df.columns), 'name_found', "")
df.insert(len(df.columns), 'relevance', 0.0)
df.insert(len(df.columns), 'relevance_names', False)
for term in relevance_filter_terms:
    df.insert(len(df.columns), 'relevance_'+term, False)


def common_name_preproc(row):
    return df_names.loc[row.id-1, 'common_name']


def scientific_name_preproc(row):
    return df_names.loc[row.id-1, 'scientific_name']


if 'common_name' not in df.columns:
    df.insert(1, 'common_name', "")
    df.insert(2, 'scientific_name', "")
    print()
    print('Adding common name column...')
    df.common_name = df.progress_apply(common_name_preproc, axis=1)
    print()
    print('Adding scientific name column...')
    df.scientific_name = df.progress_apply(scientific_name_preproc, axis=1)


# Searching for names
def search_for_names(row):
    for col in columns_to_search:
        col_data = row[col]
        if col_data is not None:
            col_data = col_data.lower()
            idx = row.id
            if idx < 158:  # all ADB names has id <158, otherwise this is something we have added like name=exoticpets
                for nm in df_names.loc[idx-1, 'all_names']:
                    if nm in col_data:
                        row.relevance_names = True
                        row.name_found = nm
                        return row
            else:
                for species_names in df_names['all_names'][:157]:  # exoticpets (id=157) excluded from the name search in posts
                    for nm in species_names:
                        if nm in col_data:
                            row.relevance_names = True
                            row.name_found = nm
                            return row
    return row


print()
print('Searching for names...')
df = df.progress_apply(search_for_names, axis=1)


# Searching for relevance_filter_terms
def search_for_filter_terms(row):
    for col in columns_to_search:
        col_data = row[col]
        if col_data is not None:
            col_data = col_data.lower()
            for term in relevance_filter_terms:
                if term in col_data:
                    row['relevance_'+term] = True
    return row


print()
print('Searching for relevance_filter_terms...')
df = df.progress_apply(search_for_filter_terms, axis=1)


# Calculating relevance scores
filter_terms_weight = 0.5/len(relevance_filter_terms)


def calculate_scores(row):
    score = row['relevance_names'] * 0.5
    for term in relevance_filter_terms:
        score += row['relevance_'+term] * filter_terms_weight
    row['relevance'] = score
    return row


print()
print('Calculating relevance scores...')
df = df.progress_apply(calculate_scores, axis=1)


# Saving data
dirName = path/'filtered'

# Create target directory & all intermediate directories if don't exists
dirName.mkdir(parents=True, exist_ok=True)

fname = path/'filtered'/'Tiktok_filtered'

print('Saving data...')
df.to_json(fname.with_suffix('.json'), orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
df = df.applymap(
    lambda x: str(x).replace('\n', ' '))  # Replace newlines to not affect importing them in Spreadsheets
df = df.applymap(lambda x: str(x).replace('\r', ' '))
df.to_csv(fname.with_suffix('.csv'), encoding='utf-8-sig', sep='ζ', index=False)

