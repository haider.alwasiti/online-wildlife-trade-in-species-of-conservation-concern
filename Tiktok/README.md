## Preparations 


Create a conda environment and clone or download this project.

```python
cd TikTok 
pip install -r requirements.txt 
python -m playwright install
```



## Output Merging 

#### usage example: 

```python
python output_merger.py
```

## Duplicates cleaning

#### usage example:
```python duplicates_cleaner.py```

## Annotator 
```
usage: 
annotator.py [-h] --annotate_encrypted_files ANNOTATE_ENCRYPTED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --annotate_encrypted_files ANNOTATE_ENCRYPTED_FILES, -enc ANNOTATE_ENCRYPTED_FILES
                        Annotate the encrypted files. This argument is
                        required.
```
#### usage example:
```python annotator.py --annotate_encrypted_files False```


## Columns post processor
```
usage: 
columns_post_processor.py [-h] --process_annotated_files
                                 PROCESS_ANNOTATED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --process_annotated_files PROCESS_ANNOTATED_FILES, -annot PROCESS_ANNOTATED_FILES
                        Process annotated files. This argument is required.
```
                        
                      
#### usage example: 
```python columns_post_processor.py --process_annotated_files True```





