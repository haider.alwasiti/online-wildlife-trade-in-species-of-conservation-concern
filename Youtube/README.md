## Preparations 


Create a conda environment and clone or download this project.

```python
cd Youtube 
pip install -r requirements.txt
```


## Data Collection


```
usage: 
youtube_scraper.py [-h] [--row_start ROW_START] [--row_end ROW_END]
                          [--pages_to_scrape PAGES_TO_SCRAPE]
                          [--delete_previous_versions DELETE_PREVIOUS_VERSIONS]


optional arguments:
  -h, --help            show this help message and exit
  --row_start ROW_START, -s ROW_START
                        start scraping at a specified row number (default: 1) of the names.csv list in the input folder
  --row_end ROW_END, -e ROW_END
                        end scraping at the specified row number [not including the ROW_END] (default: 545) 
  --repetitions REPITITIONS, -rep REPITITIONS
                        Repeat searching with the same search terms but added keywords from a second list saved in input/filter_keywords.csv in the keyword column. (default: True).
  --hash HASH, -hs HASH 
                        Hash sensitive info.
  --deleteLocal DELETELOCAL, -del DELETELOCAL 
                        Delete local json and csv files and only save in Allas. (Default False)

```

* Create `credentials.json` file inside the `Youtube` folder with the following creds (fill your CSC user's account deails in the OS_ creds + add your twitter api + add your postgreSQL details)

```json
{
  "Google_API_KEY1":"",
  "Google_API_KEY2":"",
}
```



#### usage example: 

```python
python youtube_scraper.py --row_start 1 --row_end 2
```



## Output Merging 

#### usage example: 

```python
python output_merger.py
```

## Duplicates cleaning

#### usage example:
```python duplicates_cleaner.py```

## Annotator 
```
usage: 
annotator.py [-h] --annotate_encrypted_files ANNOTATE_ENCRYPTED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --annotate_encrypted_files ANNOTATE_ENCRYPTED_FILES, -enc ANNOTATE_ENCRYPTED_FILES
                        Annotate the encrypted files. This argument is
                        required.
```
#### usage example:
```python annotator.py --annotate_encrypted_files False```


## Columns post processor
```
usage: 
columns_post_processor.py [-h] --process_annotated_files
                                 PROCESS_ANNOTATED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --process_annotated_files PROCESS_ANNOTATED_FILES, -annot PROCESS_ANNOTATED_FILES
                        Process annotated files. This argument is required.
```
                        
                      
#### usage example: 
```python columns_post_processor.py --process_annotated_files True```





