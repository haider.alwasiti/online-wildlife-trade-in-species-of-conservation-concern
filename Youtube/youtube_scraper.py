import hashlib
from pathlib import Path

import pandas as pd
import re
import json
import requests
import time
import argparse
import sys

import os
import time
from datetime import datetime, timezone, timedelta

import googleapiclient.discovery
import googleapiclient.errors



# System call to enable colored text display in Windows terminal
os.system("")



parser = argparse.ArgumentParser()
parser.add_argument('--row_start', '-s', dest='row_start', type=int, default=1,
                    help='start scraping at a specified '
                         'row number (default: 1) from the ADB names list csv file in the input folder', required=False)
parser.add_argument('--row_end', '-e', dest='row_end', type=int, default=163,
                    help='end scraping at the specified '
                         'row number [not including the ROW_END] (default: 163) from the ADB names list csv file in '
                         'the input folder',
                    required=False)
parser.add_argument('--repetitions', '-rep', dest='repetitions', type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=True,
                    help='Repeat searching with the same search terms but added keywords from a second list saved in input/filter_keywords_expanded.json in the keyword column. (default: True).', required=False)
parser.add_argument('--hash', '-hs', dest='hash', type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=True,
                    help='Hash sensitive info.', required=False)
parser.add_argument('--deleteLocal', '-del', dest='deleteLocal', type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=False,
                    help='Delete local json and csv files and only save in Allas.', required=False)

args = parser.parse_args()

row_start = args.row_start
row_end = args.row_end  # not including
row_start = row_start - 1  # convert ids into row numbers
row_end = row_end - 1  # not including

do_repetitions = args.repetitions
is_hash = args.hash
delete_local = args.deleteLocal

api_service_name = "youtube"
api_version = "v3"

class LoadCreds():
    API_IDX = 1  # Class variable that will be changed with each object instantiation

    def __init__(self):
        self.API_KEY_COUNT = None
        self.credentials = None
        self.youtube_conn = None
        self.load_json_creds()


    def load_json_creds(self):
        with open('credentials.json') as file:
            self.credentials = json.load(file)
        # Counting how many api index we have
        ks = [key for key in self.credentials.keys()]
        self.API_KEY_COUNT = 0
        for i in range(1, 1000):
            if 'Google_API_KEY' + str(i) not in ks:
                self.API_KEY_COUNT = i - 1
                print(f'Number of API keys found in credentials.json = {self.API_KEY_COUNT}')
                if self.API_KEY_COUNT == 0:
                    print(
                        f"There should be at least one Google_API_KEY to access official youtube data api. "
                        f"The key should be named 'Google_API_KEY1' and 'Google_API_KEY2', ...etc (max: 999 keys)")
                    raise SystemExit(0)
                if LoadCreds.API_IDX > self.API_KEY_COUNT:
                    LoadCreds.API_IDX = 1
                break
        self.youtube_conn = self.build_youtube_api()

    def get_creds(self):
        return self.credentials

    def get_current_youtube_token(self):
        return self.credentials['Google_API_KEY' + str(LoadCreds.API_IDX)]

    def get_next_youtube_token(self):
        LoadCreds.API_IDX += 1
        self.load_json_creds()
        return self.get_current_youtube_token()

    def build_youtube_api(self):
        api_key = self.get_current_youtube_token()
        youtube_conn = googleapiclient.discovery.build(api_service_name, api_version, developerKey=api_key)
        return youtube_conn

creds = LoadCreds()

def integer_hash(data):
    """Return a BLAKE2 hash of data, in integer representation."""
    if not isinstance(data, str):
        data = str(data)
    return int.from_bytes(
        hashlib.blake2b(data.encode("UTF-8"), digest_size=7).digest(), "big"
    )

def yt_search(search_term, youtube, next_page_token=None):
    if next_page_token is None:
        request = youtube.search().list(
            part="snippet",
            maxResults=50,
            order="relevance",
            q=search_term,
            type="video"
        )
    else:
        request = youtube.search().list(
            part="snippet",
            maxResults=50,
            order="relevance",
            pageToken=next_page_token,
            q=search_term,
            type="video"
        )
    response_json = request.execute()
    return response_json


def parse_json(json_response_data, id, search_term):
    publishedTime = json_response_data['snippet']['publishedAt']
    title = json_response_data['snippet']['title']
    description_snippet = json_response_data['snippet']['description']
    channel_name = json_response_data['snippet']['channelTitle']
    channel_id = json_response_data['snippet']['channelId']
    channel_link = 'https://www.youtube.com/channel/' + channel_id
    video_id = json_response_data['id']['videoId']
    youtube_link = 'https://www.youtube.com/watch?v=' + video_id
    if is_hash:
        channel_name = integer_hash(channel_name)
    return (id, search_term, publishedTime, title, description_snippet, channel_name,
            channel_id, channel_link, video_id, youtube_link)


def json_process(json_response, id, search_term):
    yt_data_ls = []
    if json_response.get('items') is not None:
        sz = len(json_response.get('items'))
    else:
        sz = 0
    print(f'loaded {sz} posts..')
    for i in range(sz):
        yd = parse_json(json_response['items'][i], id, search_term)
        yt_data_ls.append(yd)
    return yt_data_ls

def main():
    search_attempts = 0
    search_page_attempts = 0
    pd_input = pd.read_csv("input" + os.sep + "names.csv")
    path = 'output' + os.sep
    Path(path).mkdir(parents=True, exist_ok=True)

    relevance_keyword_terms = ['']   # To search also without added keywords put in the list ''


    if do_repetitions:
        # Loading filter terms
        df_filter_terms = pd.read_json(f'input{os.sep}filter_keywords_expanded.json', lines=True)
        df_filter_terms.keyword = df_filter_terms.keyword.apply(lambda x: ' ' + x)

        for name_lst in df_filter_terms.keyword:
            relevance_keyword_terms += [name_lst]


    repeat_keyword_terms = relevance_keyword_terms  # + relevance_place_terms  # in case we repeat search for places too


    for row in range(row_start, row_end):
        for species_name in [pd_input.english_name[row], pd_input.scientific_name[row], pd_input.local_name[row]]:
            id = int(pd_input.id[row])
            if pd.isna(species_name): continue  # exclude empty cells
            nms = re.sub('\\\|\.|\\""|\:|�|\)', '', species_name)
            for original_st in re.split(';|,|\/|\(',nms):
                if original_st in ['']:  continue # exclude these species names
                for rep_keyword in repeat_keyword_terms:

                    st = original_st + rep_keyword
                    search_term = st.strip()  # remove trailing and leading spaces
                    if search_term is not None:
                        repeat = True

                        def error_handler():
                            nonlocal repeat
                            repeat = True
                            _ = creds.get_next_youtube_token()
                            print(
                                f"Will wait for 1hr and try again with the next API key token: {'Google_API_KEY' + str(LoadCreds.API_IDX)}")  # rate limit of 300 requests every 15 minutes
                            time.sleep(3600)

                        while repeat:
                            yt_data = []
                            repeat = False
                            search_attempts += 1
                            print()
                            print(
                                f'Searching for: {id}. {search_term}')
                            print()
                            try:
                                response = yt_search(search_term, creds.youtube_conn)
                            except googleapiclient.errors.HttpError as e:
                                print(f'An Http Error {e.resp.status} occurred: {e.content}')
                                error_handler()

                            if not repeat:
                                processed_data = json_process(response, id, search_term)
                                yt_data += processed_data
                                pg = 0
                                print(f'page: {pg}')
                                print(f'Search query attempt with API{LoadCreds.API_IDX} credentials: {search_attempts} ')
                                # time.sleep(2)  # The rate limit is also 1 per second

                                nextPageToken = response.get('nextPageToken')
                                while nextPageToken is not None:
                                    try:
                                        response = yt_search(search_term, creds.youtube_conn, next_page_token=nextPageToken)
                                    except googleapiclient.errors.HttpError as e:
                                        print(
                                            f'An Http Error {e.resp.status} occurred: {e.content}')
                                        error_handler()
                                        break  # This is to break the inner while and fall into the outer while to restart from page 0, since error_handler() will make a new connection to youtube api and with a new api key. So we need to restart this search query from start

                                    search_attempts += 1
                                    nextPageToken = response.get('nextPageToken')
                                    processed_data = json_process(response, id, search_term)
                                    yt_data += processed_data
                                    pg += 1
                                    print(f'page: {pg}')
                                    print(f'Search query attempt with API{LoadCreds.API_IDX} credentials: {search_attempts} ')
                                    # time.sleep(2)

                        if len(yt_data) > 0:
                            df = pd.DataFrame(yt_data,
                                              columns=['id', 'search_term', 'publishedTime', 'title',
                                                       'description_snippet', 'channel_name', 'channel_id',
                                                       'channel_link', 'video_id', 'youtube_link'])

                            # Save it as json
                            data_fn = path + f'{id}-{search_term}'
                            df.to_json(data_fn+'.json', orient='records', lines=True, force_ascii=False)

                            # Save it as csv for inspection
                            df = df.applymap(
                                lambda x: str(x).replace('\n',
                                                         ' '))  # Replace newlines to not affect importing them in Spreadsheets
                            df = df.applymap(lambda x: str(x).replace('\r', ' '))

                            df.to_csv(data_fn+'.csv', encoding='utf-8-sig', index=False)
                            print(f'Total saved posts for this search = {len(df)}')


if __name__ == '__main__':
    main()
