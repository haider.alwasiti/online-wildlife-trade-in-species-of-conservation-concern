# information-retirval-news

Script to execute the pipeline which downloads news articles related to the target species 
and applies machine learning algorithms to filter using a neural network and extract 
named entities.

## Requirements
The script uses Python 3.6. <br />
It is advised to create a python virtual environment
to install and run the pipeline inside the virtual environment (instructions to set up a 
python virtual environment can be found [here](https://python.readthedocs.io/en/stable/library/venv.html))
<br />
Python specific requirements can be found in the requirements.txt file. It is advised to 
install the required libraries using pip (instructions to install pip can be found 
[here](https://pip.pypa.io/en/stable/)). <br />
Install the Python libraries by,
```bash
pip install -r requirements.txt
```

After installing all the requirements, install the spaCy model for English by,
```bash
python -m spacy download "en"
```
Download the NLTK stopword corpus from the Python console<br />
```bash
>>import nltk
>>nltk.download('stopwords')
```

The pipeline uses MongDB to save the data and process it during the execution. This requires
a MongoDB server running locally (or remotely if preferred, but you will need to configure
the remote server). Instructions to install and run a Mongo server can be found
[here](https://docs.mongodb.com/manual/installation/)
 
 
## Execution
The pipeline can be executed by the following command,
```bash
python pipelilne.py --config config_onlinenews.json --cites_path data/citesADB.csv
```
`--config` - this is the file containing all the parameters needed by the pipelilne <br />
`--cites_path` - this is the csv file in the format of the CITES Appendix list, which contains 
all the target species which are also a part of the CITES Appendix I,II and III. <br />

Post execution all the data is stored in the MongoDB database. <br />


## Additional Information

Training a new model: <br />
The model is trained on a labelled dataset ('online_news_train_data.json', file can be found with 
the other datasets shared). To train the model
on a custom new dataset, add a collection in the same MongoDB database as the one used for pipeline and 
name the collection as "labelled_data". Follow the structure in the json mentioned above. 
Following this, execute the Python file `run_model.py`. The newly trained model will be saved in the "models" 
folder. Make sure the config file is edited accordingly for the model name and database names. <br />





