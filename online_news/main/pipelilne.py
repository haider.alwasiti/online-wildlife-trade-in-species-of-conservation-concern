    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 15:44:08 2020

@author: rtwik
"""

import os
import argparse 
import pandas as pd
import json
from joblib import Parallel, delayed
import time
from datetime import datetime
from news_scrape import Web_Scrape, Data_Processor

start = time.time()

parser = argparse.ArgumentParser()
parser.add_argument('--config', help='path to config json', type=str, required=True)
parser.add_argument('--cites_path', help='path to cites csv', type=str, required=True)

args = parser.parse_args()

with open(args.config, 'r') as f:
    config = json.load(f)
    
config['model_filepath'] = os.getcwd() + '/models/' + config['model_filepath']

DP = Data_Processor() 
WS = Web_Scrape(database_name=config['database_name'], collection_name=config['article_collection'],
                mongo_env=config['mongo_env'])

art_counts_pre = WS.get_article_count()

data_cites = pd.read_csv(args.cites_path)

eng_keywords = DP.get_keywords_from_cites(data_cites, 'EnglishNames')
google_queries = DP.make_google_queries(eng_keywords, 'EnglishNames', config['keyword_source'], 
                                        config['query_type'])


l = len(google_queries)
n = config['n_batch_split']

print('=============processing cites')

results = Parallel(n_jobs=n, backend='threading')(delayed(WS.fetch_articles)(google_queries[i[0]:i[1]], config['search_engine']) \
                              for i in WS.get_index_bounds(l, n))
    
art_counts_post = WS.get_article_count()

print('=============removing null entries')

n_docs = WS.db_collection.count_documents({})
null_doc_ids = WS.get_null_doc_ids()
WS.remove_null_entries(null_doc_ids)

n_null =  n_docs - WS.db_collection.count_documents({})

# getting all the db doc ids
doc_ids = WS.get_doc_ids()

print('=============removing blanks')
results = Parallel(n_jobs=n, backend='threading')(delayed(WS.remove_blank_db)(doc_ids[i[0]:i[1]]) \
                              for i in WS.get_index_bounds(len(doc_ids), n))

n_blank = art_counts_post -  WS.get_article_count()

print('=============removing non english')
results = Parallel(n_jobs=n, backend='threading')(delayed(WS.language_filter)(doc_ids[i[0]:i[1]]) \
                              for i in WS.get_index_bounds(len(doc_ids), n))

n_non_eng = art_counts_post - n_blank -  WS.get_article_count()

print('=============removing duplicates')
results = Parallel(n_jobs=n, backend='threading')(delayed(WS.deduplicate_db)(doc_ids[i[0]:i[1]]) \
                              for i in WS.get_index_bounds(len(doc_ids), n))

print('=============removing irrelevant')
WS.remove_irrelevant_db(doc_ids, model_params=config)

print('===============extracting NLP info')
WS.initiate_ner_model(model_name=config['spacy_model'])
results = Parallel(n_jobs=n, backend='threading')(delayed(WS.extract_ner_db)(doc_ids[i[0]:i[1]]) \
                              for i in WS.get_index_bounds(len(doc_ids), n))

report = {'time taken (mins)' : (time.time()- start)/60 ,
          'new articles added' :  WS.get_article_count() - art_counts_pre,
          'null removed':  n_null,
          'blanks removed' : n_blank,
          'non english removed': n_non_eng,
          'total urls': len(WS.get_seen_urls()),
          'total articles' :  WS.get_article_count(),
          'total documents': WS.db_collection.count_documents({}),
          'date': datetime.today().strftime('%Y-%m-%d')}

WS.db_log.insert_one(report)
print(report)