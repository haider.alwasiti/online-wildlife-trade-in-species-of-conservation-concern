#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 16:29:09 2020

The code trains an image recognition model to detect pangolin images scraped
from tweets. Inputs are extracted from VGG16 model.
@author: rtwik
"""

import os
from data_processor import DataGen, Preprocessor
from model_functions import Model_functions, Metrics
import numpy

def train_and_save_model(model_func, params):
    ''' train, evaluate and save best model'''
    
    met = Metrics()
    
    params['db_source'] = 'annotated'

    database_name = 'article_database'
    collection_name = 'labelled_data'
    
    dg_train = DataGen(database_name, collection_name, {'train_test': 'train'}, params=params, **params)    
    dg_test = DataGen(database_name, collection_name, {'train_test': 'test'}, params=params, **params)    
    dg_predict = DataGen(database_name, collection_name, {'train_test': 'test'}, params=params, **params)

    model = model_func.compile_model(**params)

    model = model_func.train_model(model, dg_train, dg_test, **params)

    del(model)

    model = model_func.load_saved_model(**params)

    score = model_func.evaluate_model(model, dg_test)

    predictions = model_func.get_predictions(model, dg_predict)
    
    targets = numpy.concatenate([a[1] for a in dg_test])
    
    precision, recall, fscore, support = met.get_prec_rec_fscore(targets, predictions)
    f_metric = (precision, recall, fscore, support)
    
    return score , f_metric


def get_predictions_form_db(model_func, params):
    
    params['db_source'] = 'scraped'
    database_name = 'article_database'
    collection_name = 'animal_news'
    
    dg_predict = DataGen(database_name, collection_name, {}, mode='predict', params=params, **params)
    dg_predict_raw = DataGen(database_name, collection_name, {}, mode='raw', params=params, **params)

    model = model_func.load_saved_model(**params)
    
    predictions = model_func.get_predictions(model, dg_predict)
    raw_inputs = [i[0] for i in dg_predict_raw]
    
    return predictions, raw_inputs


# setting operational folders
CURRENT_DIR = os.getcwd()
DATA_DIR = CURRENT_DIR + '/data/'
MODELS_DIR = CURRENT_DIR + '/models/'


# instantiating objects for pre-processing, labelling data and eval metrics
# and model functions

model_func = Model_functions()
PR = Preprocessor()

add_stops = ['one', 'two', 'three', 'say', 'says', 'said', 'also', 'according',
             'year', 'years', 'also', 'another', 'first', 'second', 'third', 'could', 
             'would', 'new', 'time', 'many', 'may', 'even', 'well', 'still', 'got']

n_feat = 1000
params = {'batch_size': 50,
          'data_dir': DATA_DIR,
          'n_features': n_feat,
          'epochs': 10,
          'top_n_words': 50,
          'maxlen': n_feat,
          'n_classes': 2, 
          'char_level': False, 
          'keep_num' : False,
          'model_filepath': MODELS_DIR + 'wl_doc_classifier.h5',
          'mongo_env': 'local',
          'db_source': '',  #'scraped' for prediction, 'annotated' for training
          'add_stops': add_stops}


#--------train,test,save model---------
score, f_metric = train_and_save_model(model_func, params)
print(score, f_metric)

# #--------get predictions for db articles----
predictions, raw = get_predictions_form_db(model_func, params)

