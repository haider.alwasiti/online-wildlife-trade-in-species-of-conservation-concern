#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 21:13:47 2020

@author: rtwik
"""
import os
import pandas as pd
import re
from collections import Counter
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import HashingVectorizer
from keras.utils import Sequence
import numpy
from pymongo import MongoClient


class Preprocessor(object):
    '''methods to preprocess text data (tokenise, clean up etc)'''
    
    def __init__(self, lang='english', top_n_words=0, n_features=0, maxlen=0,
                 add_stops=[], char_level=False, keep_num=False, **extras):
        
        self.stops = set(stopwords.words(lang))
        self.hash_vectorizer = HashingVectorizer(n_features=n_features)
        self.top_n_words = top_n_words
        self.maxlen = maxlen
        self.add_stopwords(add_stops)
        self.keep_num = keep_num
        self.char_level = char_level

    def alphanumeric_and_split_text(self, text, char_level, keep_stops=False, keep_num=True):
            '''filter text to keep only alpha-numeric characters and split sentences
               into words'''
               
            if not keep_stops:
                stops = self.stops
            else:
                stops = []
                
            if keep_num:
                text = re.sub('[^a-zA-Z0-9\']', ' ', str(text))
            else:
                text = re.sub('[^a-zA-Z\']', ' ', str(text))
            
            words = text.split()
            words = [w.lower() for w in words if w.lower() not in stops]
    
            if char_level:
                words = self.get_chars(words, char_len=3)
    
            return words
    
    def get_chars(self, clean_word_list, char_len):
        '''return text as char units of len char_len, word bounds marked by #'''
        
        text = ''.join(['#' + w +'#' for w in clean_word_list]) #'.join(clean_word_list)
        #text = '#' + text + '#'
     
        chars = [text[i: i + char_len] for i in range(len(text)-(char_len-1))]

        return chars
    
    def get_split_text(self, text_list):
        '''returns each piece of text in text_list as word split list based on
           preprocessing params'''
        
        return [self.alphanumeric_and_split_text(t ,char_level=self.char_level, keep_num=self.keep_num) \
                for t in text_list]
    
    def get_word_freq(self, words):
        '''returns a sorted list of words with absolute counts'''
        
        count = Counter(words)
        return [(w,count[w]) for w in sorted(count, key=count.get, reverse=True)]
    
    def get_top_words(self, text):
        '''returns top n words based on frequency of text.
        text is a list of words in the article'''
        
        return [self.get_word_freq(w)[:self.top_n_words] for w in text]
    
    def add_stopwords(self, new_stops):
        '''appends the stopwords with custom words'''
        
        self.stops = self.stops | set([s.lower() for s in new_stops])
        
    def make_text_for_vectorisation(self, text_list):
        '''mini pipleline to convert each article from text_list ready to be
           vectorised'''

        text = self.get_split_text(text_list)
        words = self.get_top_words(text)
        
        return [' '.join([i[0] for i in w]) for w in words]
        
    def vectorize_data(self, data_X, data_Y):
        '''transforms text into hashed vectors'''
        
        X = self.hash_vectorizer.transform(data_X).toarray()

        return (X, numpy.array(data_Y, dtype=int))

    def make_train_test_split(self, data_frame, ratio, merged=True):
        ''' splits the data into train and test based on ratio'''
        data_frame = data_frame.sample(frac=1)
        data_train = data_frame.iloc[:int(len(data_frame)*ratio)]
        data_test = data_frame.iloc[int(len(data_frame)*ratio):]
        
        if merged:
            train = ['train'] * len(data_train)
            test = ['test'] * len(data_test)
            
            data_train['train_test'] = train
            data_test['train_test'] = test
            data_merged = pd.concat([data_train, data_test])
            
            return data_merged
        else:
            return data_train, data_test

class DataGen(Sequence, Preprocessor):
    '''data generator to stream data as mongodb to models'''
    
    def __init__(self, database_name, collection_name, find_filter={}, n_classes=0, batch_size=0, 
                 char_level=False, keep_num=False, mode='train', mongo_env='local',
                 params={}, db_source='', **extras):
        
        Preprocessor.__init__(self, **params)
        self.n_classes = n_classes
        self.batch_size = batch_size
        
        if mongo_env == 'local':
            self.client = MongoClient('mongodb://localhost:27017/')
        else:
            mongo_docker_port = os.environ['MONGO_PORT_27017_TCP_ADDR'] + ':' + os.environ['MONGO_PORT_27017_TCP_PORT']
            self.client = MongoClient(mongo_docker_port)
        
        self.db = self.client[database_name]
        self.find_filter = find_filter  # filter function as a dict for mongodb
        self.db_collection= self.db[collection_name]
        self.data_len = self.db_collection.count_documents(self.find_filter)
        self.keep_num = keep_num
        self.char_level = char_level
        self.mode = mode
        self.db_source = db_source
        
        if self.data_len % self.batch_size == 0:
            self.n_iter = int(self.data_len / self.batch_size) 
        else:
            self.n_iter = int(self.data_len / self.batch_size) + 1


    def __len__(self):
        '''calculates number of iterations per epoch'''
               
        return self.n_iter

    def __data_generation(self, index):
        '''generate a batch of data'''
        
        start_ind = index * self.batch_size
        end_ind = (index + 1) * self.batch_size
        if end_ind > self.db_collection.count_documents(self.find_filter):
            end_ind = self.db_collection.count_documents(self.find_filter)
            
        cursor = self.db_collection.find(self.find_filter)[start_ind: end_ind]
        
        data_points = self.get_processed_data(cursor)
        
        text = self.make_text_for_vectorisation([i[0] for i in data_points])
    
        labels = [l[1] for l in data_points]
        
        return text, labels
    
    def __getitem__(self, index):
        '''compile batch to create input-output pairs'''
        
        text, label = self.__data_generation(index)

        if self.mode in ['train', 'test']:
            return self.vectorize_data(text, label)
        elif self.mode == 'predict':
            return self.vectorize_data(text, label)[0]
        elif self.mode == 'raw':
            return (text, label)
        
        
    def get_processed_data(self, cursor):
        ''' returns data for down stream vectorisation based on database source'''
        
        if self.db_source == 'annotated':
            data_points = [(i['articles'], i['label']) for i in cursor]
            
        if self.db_source == 'scraped':
            data_points = [(i['articles'][j]['text'], -1) for i in cursor for j in i['articles']]
            
        return data_points
    
class Binary_data(Preprocessor):
    ''' methods to create binary labelled datasets'''
    
    def __init__(self):
        Preprocessor.__init__(self)

    def make_binary_labels(self, data_0, data_1):
        '''prepare 0-1 labels for respective datasets'''
        
        labels_0 = [0]*len(data_0)
        labels_1 = [1]*len(data_1)
        
        return labels_0, labels_1

    def make_binary_labelled_data(self, data_0, data_1):
        '''attach binary labels to respective datasets'''
        
        labels_0, labels_1 = self.make_binary_labels(data_0, data_1)

        data_frame = pd.concat([data_0, data_1])
        data_frame['label'] = labels_0 + labels_1
        data_frame = data_frame.sample(frac=1)
       
        return data_frame
