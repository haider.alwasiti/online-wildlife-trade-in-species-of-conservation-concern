## Preparations 


Create a conda environment and clone or download this project.

```python
cd Google 
pip install -r requirements.txt 
```

* You need to get a Google API key by visiting: https://console.developers.google.com/apis

* Create a project
* then "Credentials"
* then "Create credentials"
* then "API key"
* then copy the key 
* Copy the Key
* Paste your Google API KEY into the project by creating `credentials.json` file in this form:

```python
{"Google_API_KEY":"XXXXXXXXX"} 
```
## Data Collection


```
usage: 
google_scraper.py [-h] [--row_start ROW_START] [--row_end ROW_END]
                         [--pageLimit PAGELIMIT] [--country COUNTRY] --affix AFFIX
                         [--delete_previous_versions DELETE_PREVIOUS_VERSIONS]


optional arguments:
  -h, --help            show this help message and exit
  --row_start ROW_START, -s ROW_START
                        start scraping at a specified row number (default: 1)
                        from the ADB names list csv file in the input folder
  --row_end ROW_END, -e ROW_END
                        end scraping at the specified row number [not
                        including the ROW_END] (default: 163) from the ADB
                        names list csv file in the input folder
  --pageLimit PAGELIMIT, -p PAGELIMIT
                        How many pages to read from a search result (each with
                        10 results)
  --country COUNTRY, -c COUNTRY
                        country parameter for the Google search API to
                        restrict search for only specific country or all the
                        world. If search for only Philippines (--country
                        countryPH). If search needed for all the world ignore
                        this argument or pass None. Default: None. For all
                        possible values, see: https://metacpan.org/pod/WWW::Go
                        ogle::CustomSearch#Country-Collection-Values-(cr)
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is needed then
                        affix can be ph or PH. This argument is required.
  --delete_previous_versions DELETE_PREVIOUS_VERSIONS, -del DELETE_PREVIOUS_VERSIONS
                        Delete previous redundant data scraped files (default:
                        True).
```

#### Scrape for Philippines only: 

```python
python google_scraper.py --row_start 1 --row_end 2 --country countryPH --affix PH --pageLimit 1
```

#### Scrape for World: 

```python
python google_scraper.py --row_start 1 --row_end 2 --affix WORLD --pageLimit 1
```

## Output Merging 

```
usage: 
output_merger.py [-h] --affix AFFIX


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is needed then
                        affix can be ph or PH. This argument is required.
```
#### Philippines: 
```python output_merger.py --affix PH```
#### World: 
```python output_merger.py --affix WORLD```



###Duplicates cleaning
```
usage: 
duplicates_cleaner.py [-h] --affix AFFIX


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is specified in bbox then
                        affix can be ph or PH. This argument is required.
```

#### Philippines: 
```python duplicates_cleaner.py --affix PH```
#### World: 
```python duplicates_cleaner.py --affix WORLD```

## Web links scraper (method 1) 
```
usage: 
web_links_scraper.py [-h] --affix AFFIX
                            [--retry_skipped_downloads RETRY_SKIPPED_DOWNLOADS]
                            [--max_scroll_tries MAX_SCROLL_TRIES]
                            [--workers WORKERS]


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is needed then
                        affix can be ph or PH. This argument is required.
  --retry_skipped_downloads RETRY_SKIPPED_DOWNLOADS, -r RETRY_SKIPPED_DOWNLOADS
                        Retry loading webpages skipped to load in a previous
                        run (default: True)
  --max_scroll_tries MAX_SCROLL_TRIES, -m MAX_SCROLL_TRIES
                        maximum number of scroll to end swipes to let the JS
                        code running on all the webpage to load data (default:
                        5)
  --workers WORKERS, -w WORKERS
                        number of workers running in parallel (default:
                        2*number of cpu cores of this machine)
```
#### Philippines: 
```python web_links_scraper.py --retry_skipped_downloads True --workers 2 --affix PH ```
#### World: 
```python web_links_scraper.py --retry_skipped_downloads True --workers 2 --affix WORLD```


## Web links scraper (method 2) 
```
usage: 
web_links_scraper2.py [-h] --affix AFFIX
                            [--retry_skipped_downloads RETRY_SKIPPED_DOWNLOADS]
                            [--max_scroll_tries MAX_SCROLL_TRIES]
                            [--workers WORKERS]


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is needed then
                        affix can be ph or PH. This argument is required.
  --retry_skipped_downloads RETRY_SKIPPED_DOWNLOADS, -r RETRY_SKIPPED_DOWNLOADS
                        Retry loading webpages skipped to load in a previous
                        run (default: True)
  --max_scroll_tries MAX_SCROLL_TRIES, -m MAX_SCROLL_TRIES
                        maximum number of scroll to end swipes to let the JS
                        code running on all the webpage to load data (default:
                        5)
  --workers WORKERS, -w WORKERS
                        number of workers running in parallel (default:
                        2*number of cpu cores of this machine)
```
#### Philippines: 
```python web_links_scraper2.py --retry_skipped_downloads True --workers 2 --affix PH ```
#### World: 
```python web_links_scraper2.py --retry_skipped_downloads True --workers 2 --affix WORLD```


## Annotator 
```
usage: 
annotator.py [-h] --affix AFFIX --annotate_encrypted_files
                    ANNOTATE_ENCRYPTED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is needed then
                        affix can be ph or PH. This argument is required.
  --annotate_encrypted_files ANNOTATE_ENCRYPTED_FILES, -enc ANNOTATE_ENCRYPTED_FILES
                        Annotate the encrypted files. This argument is
                        required.
```
#### Philippines: 
```python annotator.py --annotate_encrypted_files False --affix PH```
#### World: 
```python annotator.py --annotate_encrypted_files False --affix WORLD```

## Columns post processor
```
usage: 
columns_post_processor.py [-h] --affix AFFIX --process_annotated_files
                                 PROCESS_ANNOTATED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is needed then affix can
                        be ph or PH. This argument is required.
  --process_annotated_files PROCESS_ANNOTATED_FILES, -annot PROCESS_ANNOTATED_FILES
                        Process annotated files. This argument is required.
```
                        
                      
#### Philippines: 
```python columns_post_processor.py --process_annotated_files True --affix PH```
#### World: 
```python columns_post_processor.py --process_annotated_files True --affix WORLD```




