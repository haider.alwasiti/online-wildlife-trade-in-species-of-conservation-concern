
import hashlib
from pathlib import Path

import pandas as pd
import re
import json
import requests
import time
import argparse
import sys

sys.path.append("..")
from utils.tools import *
from utils.csc import *
import os
import time
from datetime import datetime, timezone, timedelta

# System call to enable colored text display in Windows terminal
os.system("")

parser = argparse.ArgumentParser()
parser.add_argument('--row_start', '-s', dest='row_start', type=int, default=1,   
                    help='start scraping at a specified '
                         'row number (default: 1) from the ADB names list csv file in the input folder', required=False)
parser.add_argument('--row_end', '-e', dest='row_end', type=int, default=163,  
                    help='end scraping at the specified '
                         'row number [not inclusive]. See names list csv file in '
                         'the input folder',
                    required=False)
parser.add_argument('--tweetLimit', '-p', dest='tweetLimit', type=int, default=1000000000,
                    help='How many tweets to read from a search result (default: 1G)', required=False)
parser.add_argument('--delay', '-c', dest='delay', type=int, default=1000,
                    help='delay in milliseconds between each query and another (default: 1000)', required=False)
parser.add_argument('--since', '-sc', dest='since', type=str, default="2013-01-14T16:20:30", 
                    help='scraping process ending at an oldest tweet dated with this date in UTC time '
                         '[e.g.: 2006-04-01T00:00:00] (default 2006-04-01T00:00:00 UTC time)', required=False)
parser.add_argument('--until', '-ut', dest='until', type=str, default="",  
                    help='scraping process starting from tweet dated with this date and continue scraping in backward '
                         '(scrape older tweets than this date) '
                         '[e.g.: 2022-04-01T00:00:00] (default NOW-1hr UTC where the value is "")', required=False)
parser.add_argument('--repetitions', '-rep', dest='repetitions', type=lambda s: s.lower() in ['true', 't', 'yes', '1'],
                    default=False,   
                    help='Repeat searching with the same search terms but added keywords from a second list saved in input/filter_keywords_expanded.json in the keyword column. (default: True).',
                    required=False)
parser.add_argument('--hash', '-hs', dest='hash', type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=False,
                    help='Hash sensitive info.', required=False)
parser.add_argument('--deleteLocal', '-del', dest='deleteLocal', type=lambda s: s.lower() in ['true', 't', 'yes', '1'],
                    default=False,
                    help='Delete local json and csv files and only save in Allas.', required=False)


args = parser.parse_args()

row_start = args.row_start
row_end = args.row_end  
row_start = row_start - 1  
row_end = row_end - 1  

tweetLimit = args.tweetLimit  # number of tweets to read from a search result.
delay = args.delay  # delay in milliseconds between each query and another
since = args.since
start_time = since + ".000Z"
until = args.until
if until == "":  # to prevent api error that this is in the future by 1 sec or something
    end_time = (datetime.now(timezone.utc) - timedelta(hours=1)).strftime("%Y-%m-%dT%H:%M:%S.000Z")
else:
    end_time = until + ".000Z"
do_repetitions = args.repetitions
is_csc = args.csc
is_hash = args.hash
delete_local = args.deleteLocal

def integer_hash(data):
    """Return a BLAKE2 hash of data, in integer representation."""
    if not isinstance(data, str):
        data = str(data)
    return int.from_bytes(
        hashlib.blake2b(data.encode("UTF-8"), digest_size=7).digest(), "big"
    )



def df_json_save_append(df, filename):
    """
    Lazy save of large dataframes by appending small df chunks to json file:
    Saves the dataframe as json if file not exist
    else append the saving into the file
    file handle will be positioned to last and to maintain Json structure [ be replaced by ,
    """
    df.to_json('temp.json', orient='records', lines=True, force_ascii=False)
    a = ''
    with open('temp.json', encoding="utf-8") as temp_file:
        for line in temp_file:
            a += line
    if os.path.isfile(filename):
        with open(filename, mode="a+", encoding="utf-8") as f:
            f.write(a)
    else:
        with open(filename, mode="w", encoding="utf-8") as f:
            f.write(a)
    os.remove('temp.json')


class LoadCreds():
    API_IDX = 1 

    def __init__(self):
        self.API_KEY_COUNT = None
        self.credentials = None
        self.load_json_creds()

    def load_json_creds(self):
        with open('credentials.json') as file:
            self.credentials = json.load(file)
        # Counting how many api index we have
        ks = [key for key in self.credentials.keys()]
        self.API_KEY_COUNT = 0
        for i in range(1, 1000):
            if 'Twitter_Bearer_Token' + str(i) not in ks:
                self.API_KEY_COUNT = i - 1
                print(f'Number of API keys found in credentials.json = {self.API_KEY_COUNT}')
                if self.API_KEY_COUNT == 0:
                    print(
                        f"There should be at least one Twitter_Bearer_Token to access official twitter api v3. "
                        f"The key should be named 'Twitter_Bearer_Token1' and 'Twitter_Bearer_Token2', ...etc (max: 999 keys)")
                    raise SystemExit(0)
                if LoadCreds.API_IDX > self.API_KEY_COUNT:
                    LoadCreds.API_IDX = 1
                break

    def get_creds(self):
        return self.credentials

    def get_current_twitter_token(self):
        return self.credentials['Twitter_Bearer_Token' + str(LoadCreds.API_IDX)]

    def get_next_twitter_token(self):
        LoadCreds.API_IDX += 1
        self.load_json_creds()
        return self.get_current_twitter_token()


creds = LoadCreds()

search_url = "https://api.twitter.com/2/tweets/search/all"

tweetfields = ",".join(["id", "author_id", "conversation_id", "created_at", "lang", "text", "public_metrics", "geo",
                        "entities"])  # entities extract: hashtags, urls
placefields = ",".join(["country_code"])
expansions = ",".join(["entities.mentions.username", "geo.place_id"])
max_results = 500  # The max allowed per page = 500


def bearer_oauth(r):
    """
    Method required by bearer token authentication.
    """
    r.headers["Authorization"] = f"Bearer {creds.get_current_twitter_token()}"
    r.headers["User-Agent"] = "v2FullArchiveSearchPython"
    return r


def connect_to_endpoint(url, params):
    repeat = True

    def error_handler():
        nonlocal repeat
        repeat = True
        _ = creds.get_next_twitter_token()
        print(
            f"Will wait for 5 min. and try again with the next bearer token: Twitter_Bearer_Token{LoadCreds.API_IDX}")  # rate limit of 300 requests every 15 minutes
        time.sleep(300)

    while repeat:
        repeat = False
        try:
            response = requests.request("GET", url, auth=bearer_oauth, params=params)
        except Exception as e:
            print(f'An Exception error happened: {str(e)}')
            error_handler()
        else:
            if response.status_code != 200:
                print(f'An Http error happened: {response.status_code}, {response.text}')
                error_handler()
    return response.json()


def parse_json(json_response_data, id, search_term):
    tweet_id = json_response_data['id']
    author_id = json_response_data['author_id']
    link = "https://twitter.com/twitter/status/" + tweet_id
    created_at = json_response_data['created_at']
    tweet = json_response_data['text']
    # Removal of mentions
    tweet = ' '.join(re.sub("(@[A-Za-z0-9_]+)", " ", tweet).split())
    language = json_response_data['lang']
    coord = ''
    geo = json_response_data.get('geo')
    if geo is not None:
        place = geo.get('coordinates')
        if place is not None:
            coord = place
    country = ''
    retweet_count, reply_count, like_count, quote_count = json_response_data['public_metrics']['retweet_count'], \
                                                          json_response_data['public_metrics']['reply_count'], \
                                                          json_response_data['public_metrics']['like_count'], \
                                                          json_response_data['public_metrics']['quote_count']
    # Get the mentions ids
    mentions = []
    item = json_response_data.get('entities')
    if item is not None:
        if item.get('mentions') is not None:
            for mn in item.get('mentions'):
                mentions.append(mn.get('id'))
    # Hashing sensitive info
    if is_hash:
        tweet_id = integer_hash(tweet_id)
        author_id = integer_hash(author_id)
        hashed_mentions = []
        for mn in mentions:
            hashed_mentions.append(integer_hash(mn))
        mentions = hashed_mentions
    return (id, search_term, tweet_id, author_id, created_at, tweet, language, coord, country, retweet_count, \
            reply_count, like_count, quote_count, mentions, link)


def json_process(json_response, id, search_term):
    tweets_data_ls = []
    if json_response.get('data') is not None:
        sz = len(json_response.get('data'))
    else:
        sz = 0
    print(f'loaded {sz} tweets from API {LoadCreds.API_IDX}...')
    for i in range(sz):
        td = parse_json(json_response['data'][i], id, search_term)
        tweets_data_ls.append(td)
    return tweets_data_ls


def save_data_to_disk(processed_data, data_fn):
    if len(processed_data) > 0:
        df_processed_data = pd.DataFrame(processed_data,
                                         columns=['id', 'search_term', 'tweet_id', 'author_id', 'created_at', 'tweet',
                                                  'language',
                                                  'geo', 'country',
                                                  'retweet_count', 'reply_count', 'like_count', 'quote_count',
                                                  'mentions',
                                                  'link'])
        df_json_save_append(df_processed_data, data_fn + '.json')


def main():
    search_attempts = 0
    search_page_attempts = 0
    pd_input = pd.read_csv("input" + os.sep + "names.csv")
    path = 'output' + os.sep
    Path(path).mkdir(parents=True, exist_ok=True)


    relevance_keyword_terms = ['']  # So that we search also for species names without keywords additions


    if do_repetitions:
        # Loading filter terms
        df_filter_terms = pd.read_json(f'input{os.sep}filter_keywords_expanded.json', lines=True)

        # drop the signs £, €, $ since they are not compatible with the api as search query
        exc = ['£', '€', '$']
        df_filter_terms = df_filter_terms[~df_filter_terms.keyword.isin(exc)]
        # adding leading spaces to filtering keywords
        df_filter_terms.keyword = df_filter_terms.keyword.apply(lambda x: ' ' + x)

        for name_lst in df_filter_terms.keyword:
            relevance_keyword_terms += [name_lst]



    repeat_keyword_terms = relevance_keyword_terms 

    ids, search_terms = [], []

    for row in range(row_start, row_end):
        for species_name in [pd_input.english_name[row], pd_input.scientific_name[row], pd_input.local_name[row]]:
            id = int(pd_input.id[row])
            if pd.isna(species_name): continue  # exclude empty cells
            nms = re.sub('\\\|\.|\\""|\:|�|\)', '', species_name)
            for original_st in re.split(';|,|\/|\(', nms):
                if original_st in ['nene','tanta','tiger','nēnē','ounce','jaguar','toki','solitaire','snow leopard',
                                   'peruvian','chimp','chinchilla chinchilla','mitu mitu','baboon','panda bear',
                                   'manatee','fute','luth','bonobo','chitra chitra','panamanian','blue whale',
                                   'loggerhead sea turtle','loggerhead']:  continue  # Exclude species names
                for rep_keyword in repeat_keyword_terms:
                    st = original_st + rep_keyword
                    search_term = st.strip()  # remove trailing and leading spaces
                    if search_term is not None:
                        search_term_without_RT = search_term + " - is:retweet"  # to avoid retrieving retweets  
                        total_ds = 0
                        data_fn = path + f'{id}-{search_term}'
                        search_attempts += 1
                        print()
                        print(
                            f'Searching with the Twitter_Bearer_Token{LoadCreds.API_IDX} for: {id}. {search_term}')
                        print()

                        query_params = {'query': search_term_without_RT,  
                                        'max_results': max_results,
                                        'start_time': start_time, 'end_time': end_time, 'tweet.fields': tweetfields,
                                        'place.fields': placefields, 'expansions': expansions}

                        json_response = connect_to_endpoint(search_url, query_params)
                        processed_data = json_process(json_response, id, search_term)
                        # tweet_data += processed_data
                        save_data_to_disk(processed_data, data_fn)
                        total_ds += len(processed_data)

                        print('page: 0')
                        time.sleep(2)

                        next_token = json_response['meta'].get('next_token')
                        if next_token is not None:
                            pg = 1
                            while True:
                                query_params_paginated = query_params
                                query_params_paginated.update({'pagination_token': next_token})
                                json_response = connect_to_endpoint(search_url, query_params)
                                processed_data = json_process(json_response, id, search_term)
                                save_data_to_disk(processed_data, data_fn)
                                total_ds += len(processed_data)
                                print(f'page: {pg}')
                                time.sleep(2)

                                pg += 1
                                next_token = json_response['meta'].get('next_token')
                                if next_token is None:
                                    break
                        
                        print(f'Total saved tweets for this search = {total_ds}')



if __name__ == '__main__':
    main()
