import os
import shutil

# print with colors
# https://stackoverflow.com/a/287944/1970830
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# If filename exists add number to the file name to prevent overwrite
# Any file name will be modified to add '_v'. If this is the first file '_v1' will be added
# modified version of:
# https://stackoverflow.com/a/29019430/1970830
def build_filename(name, num=1):
    root, ext = os.path.splitext(name)
    return '%s%s%d%s' % (root, '_v', num, ext) if num else name


def find_next_filename(name, max_tries=100):
    for i in range(1, max_tries):
        test_name = build_filename(name, i)
        if not os.path.exists(test_name): return test_name
    return None


def delete_all_files(folder_path):
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            os.remove(os.path.join(root, file))


def move_files(folder_from_path, folder_to_path):
    for root, dirs, files in os.walk(folder_from_path):
        for file in files:
            shutil.move(os.path.join(root, file), os.path.join(folder_to_path, file))
