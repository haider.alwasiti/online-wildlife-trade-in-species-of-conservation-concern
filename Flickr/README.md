## Preparations 


Create a conda environment and clone or download this project.

```python
cd Flickr 
pip install -r requirements.txt 
```

* Create an account in Flickr if you don't have yet

* Get an API Key from: https://www.flickr.com/services/apps/create/apply 

* "Apply for a non-commercial key" 

* Fill the form by adding any project name like "flickr_analysis" and define what you are building like "Wild life pictures analysis" 

* Submit 

* Copy the Key and the Secret 

* Paste your Flickr API credentials into the project by creating credentials.json file 
in this form: 

```python
{"FLICKER_KEY":"XXXXXXX", "FLICKER_SECRET":"XXXXXXXX"} 
```
## Data Collection


```
usage: 
batch_flickr_scraper.py [-h] [--row_start ROW_START]
                               [--row_end ROW_END]
                               [--prev_hr_download_attempts PREV_HR_DOWNLOAD_ATTEMPTS]
                               [--number_to_download NUMBER_TO_DOWNLOAD]
                               [--download_rate DOWNLOAD_RATE] [--bbox BBOX]
                               --affix AFFIX
                               [--delete_previous_versions DELETE_PREVIOUS_VERSIONS]


optional arguments:
  -h, --help            show this help message and exit
  --row_start ROW_START, -s ROW_START
                        start scraping at a specified row number (default: 1)
                        from the ADB names list csv file in the input folder
  --row_end ROW_END, -e ROW_END
                        end scraping at the specified row number [not
                        including the ROW_END] (default: 163) from the ADB
                        names list csv file in the input folder
  --prev_hr_download_attempts PREV_HR_DOWNLOAD_ATTEMPTS, -p PREV_HR_DOWNLOAD_ATTEMPTS
                        how many Flickr search attempts you have caried out in
                        the past 60 min. (default: 0). Flickr allows 3600/hr
                        search attempts.
  --number_to_download NUMBER_TO_DOWNLOAD, -n NUMBER_TO_DOWNLOAD
                        how many search results will be carried out for each
                        search term (default: 500)
  --download_rate DOWNLOAD_RATE, -r DOWNLOAD_RATE
                        Download rate per hour (default: 3000). Flickr allows
                        3600/hr search attempts.
  --bbox BBOX, -b BBOX  Geographical bounding box to search in. Should be
                        separated by spaces like: <minimum_longitude
                        minimum_latitude maximum_longitude maximum_latitude>.
                        Longitude has a range of -180 to 180 , latitude of -90
                        to 90. For example Philippines long, lat bbox: "116.66
                        4.66 126.56 21.16". Default no bbox specified.
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is specified in bbox then
                        affix can be ph or PH. This argument is required.
  --delete_previous_versions DELETE_PREVIOUS_VERSIONS, -del DELETE_PREVIOUS_VERSIONS
                        Delete previous redundant data scraped files (default:
                        True).
```

#### Scrape for Philippines only: 

```python
python batch_flickr_scraper.py --row_start 1 --row_end 2 --number_to_download 3 --bbox "116.66 4.66 126.56 21.16" --affix PH 
```

#### Scrape for World: 

```python
python batch_flickr_scraper.py --row_start 1 --row_end 2 --number_to_download 3 --affix WORLD 
```

## Output Merging 

```
usage: 
output_merger.py [-h] --affix AFFIX


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is specified in bbox then
                        affix can be ph or PH. This argument is required.

```
#### Philippines: 
```python output_merger.py --affix PH ```
#### World: 
```python output_merger.py --affix WORLD ```



## Duplicates cleaning
```
usage: 
duplicates_cleaner.py [-h] --affix AFFIX


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is specified in bbox then
                        affix can be ph or PH. This argument is required.
```

#### Philippines: 
```python duplicates_cleaner.py --affix PH```
#### World: 
```python duplicates_cleaner.py --affix WORLD```

## Annotator 
```
usage: 
annotator.py [-h] --affix AFFIX --annotate_encrypted_files
                    ANNOTATE_ENCRYPTED_FILES




optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is specified in bbox then
                        affix can be _ph. This argument is required.
  --annotate_encrypted_files ANNOTATE_ENCRYPTED_FILES, -enc ANNOTATE_ENCRYPTED_FILES
                        Annotate the encrypted files. This argument is
                        required.
```
#### Philippines: 
```python annotator.py --annotate_encrypted_files False --affix PH```
#### World: 
```python annotator.py --annotate_encrypted_files False --affix WORLD```

## Columns post processor
```
usage: 
columns_post_processor.py [-h] --affix AFFIX --process_annotated_files
                                 PROCESS_ANNOTATED_FILES


optional arguments:
  -h, --help            show this help message and exit
  --affix AFFIX, -a AFFIX
                        affix for saving filenames and data. Example if
                        Philippines restricted area is specified in bbox then
                        affix can be ph or PH. This argument is required.
  --process_annotated_files PROCESS_ANNOTATED_FILES, -annot PROCESS_ANNOTATED_FILES
                        Process annotated files. This argument is required.
```
                        
                      
#### Philippines: 
```python columns_post_processor.py --process_annotated_files True --affix PH```
#### World: 
```python columns_post_processor.py --process_annotated_files True --affix WORLD```




