from pathlib import Path
from flickr_scraper import download_photos
import json
from utils import *
import pandas as pd
import re
import time
import math
import argparse
import os

# System call to enable colored text display in Windows terminal
os.system("")

parser = argparse.ArgumentParser()
parser.add_argument('--row_start', '-s', dest='row_start', type=int, default=1,
                    help='start scraping at a specified '
                         'row number (default: 1) from the ADB names list csv file in the input folder', required=False)
parser.add_argument('--row_end', '-e', dest='row_end', type=int, default=163,
                    help='end scraping at the specified '
                         'row number [not including the ROW_END] (default: 163) from the ADB names list csv file in '
                         'the input folder',
                    required=False)
parser.add_argument('--prev_hr_download_attempts', '-p', dest='prev_hr_download_attempts', type=int, default=0,
                    help='how many Flickr search attempts you have caried out in the past 60 min. (default: 0). '
                         'Flickr allows 3600/hr search attempts.',
                    required=False)
parser.add_argument('--number_to_download', '-n', dest='number_to_download', type=int, default=500,
                    help='how many search results will be carried out for each search term (default: 500)',
                    required=False)
parser.add_argument('--download_rate', '-r', dest='download_rate', type=int, default=3000,
                    help='Download rate per hour (default: 3000). Flickr allows 3600/hr search attempts.',
                    required=False)
parser.add_argument('--bbox', '-b', dest='bbox', required=False, type=str, default='',
                    help='Geographical bounding box to search in. Should be separated by spaces like: '
                         '<minimum_longitude minimum_latitude maximum_longitude maximum_latitude>. '
                         'Longitude has a range of -180 to 180 , latitude of -90 to 90. For example Philippines long, '
                         'lat bbox: '
                         '"116.66 4.66 126.56 21.16". Default no bbox specified.')
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is specified in '
                         'bbox then affix can be ph or PH. This argument is required.',
                    required=True)
parser.add_argument('--delete_previous_versions', '-del', dest='delete_previous_versions',
                    type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=True,
                    help='Delete previous redundant data scraped files (default: True).', required=False)

args = parser.parse_args()

row_start = args.row_start
row_end = args.row_end  # not including
prev_hr_download_attempts = args.prev_hr_download_attempts  # in the past 60 min. search attempts (Flickr allows 3600/h)
number_to_download = args.number_to_download  # per search term
download_rate = args.download_rate  # per hour
bbox = args.bbox  # "116.66 4.66 126.56 21.16"  # philippines long, lat bbox
affix = args.affix
delete_previous_versions = args.delete_previous_versions  # delete previous redundant data scraped files

path = f'output_{affix.lower()}{os.sep}'
images_output_path = f'images_{affix.lower()}{os.sep}'

Path(path).mkdir(parents=True, exist_ok=True)
Path(images_output_path).mkdir(parents=True, exist_ok=True)

temp_path = 'temp_imgs' + os.sep
fn = 'data_' + affix.lower()

search_attempts = 0
download_attempts = 0
pd_input = pd.read_csv("input" + os.sep + "names.csv")

row_start = row_start - 1  # convert rows from the CSV input row column numbers
row_end = row_end - 1  # not including

start_time = time.time()

with open('credentials.json') as file:
    credentials = json.load(file)

KEY = credentials['FLICKER_KEY']
SECRET = credentials['FLICKER_SECRET']

# Check flickr API key available
fr_key_help = 'https://www.flickr.com/services/apps/create/apply'
assert KEY and SECRET, f'Get your Flickr API key and Secret. To apply visit {fr_key_help}. Save them in a file ' \
                       f'named: credentials.json as {"KEY":"YOUR_API_KEY", "SECRET":"YOUR_API_SECRET"}'

# Split bbox parameters into valid format
try:
    bbox = bbox.split(' ')
except Exception as e:
    bbox = None
    print('Geographical bounding box unidentified -> bbox will be ignored!')
if bbox and len(bbox) != 4:
    bbox = None
    print('Geographical bounding box unidentified -> bbox will be ignored!')
if bbox:
    print(f'{bcolors.OKBLUE}{bcolors.BOLD}Searching within the Geographical bounding box: min_long, min_lat, '
          f'max_long, max_lat: {bbox}{bcolors.ENDC}')
    print()

ids = []
search_terms = []
prev_file_names = []
pd_data = pd.DataFrame()


def save_data(pd_data, path, fn, row_start, row_end, delete_previous_versions=False):
    global prev_file_names
    data_fn = path + fn + '_' + str(row_start) + '-' + str(row_end) + '.csv'
    data_fn = find_next_filename(data_fn)  # If exists add number to the file name to prevent overwrite
    data_fn_json = path + fn + '_' + str(row_start) + '-' + str(row_end) + '.json'
    data_fn_json = find_next_filename(data_fn_json)  # If exists add number to the file name to prevent overwrite
    pd_data.to_json(data_fn_json, orient='records', lines=True, force_ascii=False)

    # Saving the csv with special character separator like ζ works well with even non-english commonly used text
    pd_data_csv = pd_data.applymap(
        lambda x: str(x).replace('\n', ' '))  # Replace newlines to not affect importing them in Spreadsheets
    pd_data_csv = pd_data_csv.applymap(lambda x: str(x).replace('\r', ' '))
    pd_data_csv.to_csv(data_fn, encoding='utf-8-sig', sep='ζ', index=False)
    print(f'{bcolors.OKGREEN}Image data saved in: {data_fn}{bcolors.ENDC}')
    print()

    if delete_previous_versions:
        print()
        for nm in prev_file_names:
            if os.path.exists(nm):
                os.remove(nm)
                print(f'{bcolors.WARNING}Deleted previous file version: {nm}{bcolors.ENDC}')
        print()
    prev_file_names = [data_fn, data_fn_json]


for row in range(row_start, row_end):  # range(len(pd_input)):
    delete_all_files(temp_path)  # Delete older temporary image files

    for species_name in [pd_input.english_name[row], pd_input.scientific_name[row], pd_input.local_name[row]]:
        if isinstance(species_name, float):  # for some reason empty cells is regarded as float NaN
            if math.isnan(species_name):
                continue  # exclude empty cells
        nms = re.sub('\\\|\.|\\""|\:|�|\)', '', species_name)
        for st in re.split(';|,|\/|\(', nms):
            search_term = st.strip()  # remove trailing and leading spaces
            search_attempts += 1
            download_attempts_in_this_search = 0

            print(f'search attempt count: {search_attempts}')
            print(f'{bcolors.OKGREEN}Searching for: {row + 1}. {search_term}{bcolors.ENDC}')  # id. search_term
            download_attempts_in_this_search, pd_data_one_search_term = download_photos(search_query=search_term,
                                                                                        n=number_to_download,
                                                                                        download=True,
                                                                                        bbox_area=bbox,
                                                                                        id_species=int(
                                                                                            pd_input.id[row]),
                                                                                        img_save_folder_path=temp_path,
                                                                                        affix=affix.lower()
                                                                                        )

            if len(pd_data) == 0:
                pd_data = pd_data_one_search_term.copy()
            else:
                pd_data = pd_data.append(pd_data_one_search_term)

            download_attempts += download_attempts_in_this_search

            print(
                f'{bcolors.WARNING}Download attempts so far = {download_attempts} since {(time.time() - start_time) / 3600} hrs ago{bcolors.ENDC}')

            # Wait if download_attempts in this hour exceeds my download_rate threshold
            total_download_count = download_attempts + prev_hr_download_attempts
            hrs_passed = (time.time() - start_time) / 3600
            int_hr_passed = math.floor(hrs_passed)
            if total_download_count - (download_rate * int_hr_passed) > download_rate:
                wait_period = (1 + int_hr_passed - hrs_passed) * 3600
                print()
                print(f'{bcolors.WARNING}Waiting for {wait_period} seconds before continue...{bcolors.ENDC}')
                print()
                time.sleep(wait_period)

    # Saving the downloaded info of all the previous rows
    # If the same file name exist, it will save the file with a new version
    # Eventually to combine all the files into one csv,
    # one should combine the latest version available from each file name
    # If any error happens during data retrieving (or exceeded the free Flickr's 3600 request/hr),
    # the current downloaded row's info will be ignored (not saved).
    # Downloading can be started again with assigning the current row (last row number saved in the last file)
    # For example if the last file saved: data_1-1_v2.csv that means even the 1st row has not finished
    # so start again with row_start = 1
    # if  data_1-3_v2.csv -> start again with row_start = 3
    # When a new search attempt is started, delete all redundant previous csv files' attempt
    # For example if the last saved: data_4-6_v2.csv
    # Delete: data_4-5_v1.csv, data_4-5_v2.csv, data_4-6_v1.csv
    save_data(pd_data, path, fn, row_start + 1, row + 2)
    move_files(temp_path, images_output_path)  # copy files in the temp folder to the output folder
