import pandas as pd
import os
import math
import re
from tqdm import tqdm

tqdm.pandas()

row_start = 1  # For populating the scientific and common_names
row_end = 163  # not including

# convert rows from the CSV input row column numbers (starts with 1) into the actual row numbers (starts with 0)
row_start = row_start - 1
row_end = row_end - 1  # not including

pd_input = pd.read_csv("input" + os.sep + "names.csv")
common_name, scientific_name = {}, {}
for row in range(row_start, row_end):
    id_num = pd_input.id[row]

    for c, species_name in enumerate \
                ([pd_input.english_name[row], pd_input.scientific_name[row], pd_input.local_name[row]]):
        if isinstance(species_name, float):  # for some reason empty cells is regarded as float NaN
            if math.isnan(species_name):
                continue  # exclude empty cells
        nms = re.sub('\\\|\.|\\""|\:|�|\)', '', species_name)
        for st in re.split(';|,|\/|\(', nms):
            search_term = st.strip()  # remove trailing and leading spaces
            if c == 1:  # c = 1 -> we are looking now in scientific names
                if scientific_name.get(id_num) is None:
                    scientific_name[id_num] = [search_term]
                else:
                    scientific_name[id_num].append(search_term)
            else:
                if common_name.get(id_num) is None:
                    common_name[id_num] = [search_term]
                else:
                    common_name[id_num].append(search_term)

for i in common_name:
    res = []
    [res.append(x) for x in common_name[i] if x not in res]  # Remove duplicates of common_names
    common_name[i] = ','.join(res)  # convert list to string

for i in scientific_name:
    res = []
    [res.append(x) for x in scientific_name[i] if x not in res]  # Remove duplicates of scientific_names if any
    scientific_name[i] = ','.join(res)  # convert list to string

pd_out = pd.DataFrame(columns=['id', 'common_name', 'scientific_name'])
for i in scientific_name:
    pd_out.loc[i] = [i, common_name.get(i) if common_name.get(i) else "", scientific_name[i]]

# Generating all_names column: combined scientific and common names database
# with different combinations of character separators
pd_out.insert(len(pd_out.columns), 'all_names', "")


def nth_repl(s, old, new, n):  # Replace nth old substring into new substring
    find = s.find(old)
    # If find is not -1 we have found at least one match for the substring
    i = find != -1
    # loop until we find the nth or we find no match
    while find != -1 and i != n:
        # find + 1 means we start searching from after the last match
        find = s.find(old, find + 1)
        i += 1
    # If i is equal to n we found nth match so replace
    if i == n and i <= len(s.split(old)) - 1:
        return s[:find] + new + s[find + len(old):]
    return s


def combine_names(row):
    scientific_names = [x.lower().strip() for x in row.scientific_name.split(',') if x != ""]
    common_names = [x.lower().strip() for x in row.common_name.split(',') if x != ""]
    all_type_names = scientific_names + common_names
    new_names_lst = all_type_names.copy()
    for name in all_type_names:
        name_with_only_spaces = name.replace("-", " ")
        spaces_count = name_with_only_spaces.count(" ")
        permutations = 2 ** spaces_count
        for perm in range(permutations):
            perm_binary_string = "{:0>15b}".format(perm)[
                                 -spaces_count:]  # string of the binary repr. for only lowest spaces_count positions
            new_name = name_with_only_spaces
            shift = 1
            for c in range(
                    len(perm_binary_string)):  # move one by one on the positions of the binary string of permutation
                space_convert = perm_binary_string[c:c + 1]
                if space_convert == "1":
                    new_name = nth_repl(new_name, " ", "-", c + shift)
                    shift -= 1  # when toggle " " into "-" then the total count of " " and their positions will be decr.
            if new_name not in new_names_lst:
                new_names_lst += [new_name]
            if new_name.find("'") > -1:  # If find is not -1 we have found at least one match for the substring
                # types of apostrophes in ADB names only this: '
                # Change them also to the commonly used other types like: ‘’'´
                new_names_lst += [new_name.replace("'", "‘")]
                new_names_lst += [new_name.replace("'", "’")]
                new_names_lst += [new_name.replace("'", "´")]
                new_names_lst += [new_name.replace("'", " ")]
                new_names_lst += [new_name.replace("'", "")]
                # remove the apostrophe and the s character
                new_names_lst += [new_name.replace("'s", "")]

    # Add names without any separators like "-" or " " which are useful for hashtag searches
    for nm in all_type_names:
        nm_abstracted = nm.replace("-", "").replace("'", "").replace("_", "").replace(" ", "")
        if nm_abstracted not in new_names_lst:
            new_names_lst += [nm_abstracted]
        # The same of above but without "'s" which many post will use names ignoring it in the hashtag names
        nm_abstracted = nm.replace("-", "").replace("'s", "").replace("_", "").replace(" ", "")
        if nm_abstracted not in new_names_lst:
            new_names_lst += [nm_abstracted]

    row['all_names'] = new_names_lst
    return row


print('Generating all_names column: combined scientific and common names database with different combinations of '
      'character separators...')
pd_out = pd_out.progress_apply(combine_names, axis=1)
pd_out.to_json(f'input{os.sep}common_names.json', orient='records', lines=True, force_ascii=False)
pd_out.to_csv(f'input{os.sep}common_names.csv', encoding='utf-8-sig', sep='ζ', index=False)
