import pandas as pd
import os
import csv
from tqdm import tqdm
import argparse

csv.field_size_limit(13107200)

parser = argparse.ArgumentParser()
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is specified in '
                         'bbox then affix can be ph or PH. This argument is required.', required=True)

args = parser.parse_args()
affix = args.affix


### NOTE ###
# Saving the Flickr data in CSV is perfect. However MS Excel is sensitive to special
# characters that happens sometimes in the EXIF data, so in some rows the last column
# shown is EXIF although that does not means the next columns are empty. It is just an
# artifact of the viewer program (MS Excel)
path = f'output_{affix.lower()}{os.sep}'
combined = f"{path}data_{affix.lower()}_all"

print('Reading data input...')
data_fn = combined + ".csv"
df = pd.read_csv(data_fn, encoding='utf-8-sig', delimiter='ζ', engine='python')

# Cleaning
# search names to be dropped (turned out they are language names and not species names)
print('Removing language names from search terms...')
dropped_names = ['Tagalog', 'Waray', 'Cebuano', 'Ilocano', 'Pangasinense', 'Cuyonon', 'Bisaya']
df['search term'] = df['search term'].apply(lambda x: x.strip())  # remove leading and ending spaces
for name in tqdm(dropped_names):
    df.drop(df[df['search term'] == name].index, inplace=True)

# removing duplicated links per species id
print('Cleaning from duplicated links per species id...')
df_cleaned = pd.DataFrame(columns=df.columns)
for id in tqdm(range(1,158)):
    df_temp = df[df.id == id].copy()
    df_temp.drop_duplicates(subset=['link'], keep='first', inplace = True, ignore_index= True)
    df_cleaned = pd.concat([df_cleaned, df_temp], axis=0, ignore_index=True)

fname = f"{path}Flickr_{affix.lower()}_cleaned"

print('Saving data...')
df_cleaned.to_json(fname + '.json', orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
# Replace newlines to not affect importing them in Spreadsheets
df_cleaned = df_cleaned.applymap(lambda x: str(x).replace('\n', ' '))
df_cleaned = df_cleaned.applymap(lambda x: str(x).replace('\r', ' '))
df_cleaned.to_csv(fname + '.csv', encoding='utf-8-sig', sep='ζ', index=False)

print(f"Flickr cleaned file saved to {fname}")
